from __future__ import division
import matplotlib.pyplot as plt
from math import exp, sqrt
import random as rand

X0 = 1
a = 1.5
b = 1

#Brownian Motion
def W(t):
	W = [0]
	s = 0
	n=100000
	for i in range(1, int(n*t+1)):  
		Xi = rand.randint(-1,1)
		if Xi == 0:
			i-=1  
		else:
			s += Xi * 1/sqrt(n)
	return s

#solution of SDE
def X(t):
	return X0*exp((a-(b**2)/2)*t + b*W(t))

num_paths = 1000
for n in range(2,7):
	plt.figure()
	for p in range(num_paths):
		t = [i/(2**n) for i in range(2**(n+1) + 1)] #t<=2 
		x = [X(i) for i in t]
		plt.plot(t,x)
	plt.title("Solutions of SDE X(t) for t<=2 at timesteps of {0}".format(2**-n))
	plt.ylabel("X(t)")
	plt.xlabel("time t")
	plt.savefig("SDE_graphs/SDEsol_step={0}.ps".format(2**-n))
