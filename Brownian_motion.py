from __future__ import division
import matplotlib.pyplot as plt
import random as rand
import numpy as np
from math import sqrt, pi, exp

#for part (c)
def bell(x, mean, var):
	coeff = 1/sqrt(2*pi*var)
	num = (x-mean)**2
	denom = 2*var
	return coeff * exp(-num/denom)

def W(n,t):
	W = [0]
	s = 0
	for i in range(1, int(n*t+1)):  
		Xi = rand.randint(-1,1)
		if Xi == 0:
			i-=1  
		else:
			s += Xi * 1/sqrt(n)
			W.append(s)
			nt.append(i/n)
	return W

#part(a)
num_paths = 1000
n = 100000 #brownian motion when n->inf
T = 2.0
for i in range(num_paths):
	nt = [0]
	w = W(n,T)
	plt.plot(nt, w)
plt.xlabel("time t")
plt.ylabel("W(n,t)")
plt.title("Brownian motion W(t) vs time t<={1}".format(n,T))
plt.savefig("Brownian_motion_graphs/W[{0}paths].ps".format(num_paths))
#plt.show()

#part (b)
T = [1.0,2.0]
final_values = []
bellc = []
for t in T:
	for i in range(num_paths):
		nt = [0]
		w = W(n,t)
		final_values.append(w[len(w)-1])
		bellc.append(bell(final_values[i], 0, 1)) #for part (c) 
 
	plt.hist(final_values, bins=50, normed=True)

	mean = sum(final_values)/len(final_values)
	print "mean = " , mean
	var =  np.var(final_values, dtype=np.float64)
	print "var = " , var

	plt.plot(final_values, bellc, 'x') ##part (C)
	plt.title("histogram of brownian motion at W(t={0}), mean = {1}, variance = {2}".format(t,mean, var))
	plt.savefig("Brownian_motion_graphs/histogram_t={0}.ps".format(t))
	#plt.show()

#the histogram should be approximating a normal distrubution centered on mean=0 with variance=1
